const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/ 


//Find all the items with price more than $65.

let priceArray = []
const itemsWithPriceAbove65 = products.map((product) =>{
    
  return Object.values(product).map((item) => {
    if (typeof item.price === "string"  && parseInt(item.price.slice(1)) > 65) {
      return priceArray.push(item)
    }
    else if (Array.isArray(item)) {
       item.map((subItem) => {
        const subItemValue = Object.values(subItem)[0]
        if (subItemValue.price && parseInt(subItemValue.price.slice(1)) > 65) {
          return priceArray.push(subItemValue)
        }
        
      })
    }
    
  })
}
)

console.log(priceArray)



//Find all the items where quantity ordered is more than 1.

let quantityArray =[]
const itemWithQuanttity = products.map((product)=>{
    return Object.values(product).map((item)=>{
        if (typeof item.quantity === "number"  && item.quantity > 1) {
            return quantityArray.push(item)
        }
        else if (Array.isArray(item)){
            item.map((subItem)=>{
                const subItemQuantity = Object.values(subItem)[0]
                if( subItemQuantity.quantity>1){
                    return quantityArray.push(subItemQuantity)
                }
            })

        }
    })
})

console.log(quantityArray)


//Get all items which are mentioned as fragile.
let fragileArray =[]
const itemWithFragile = products.map((product)=>{
    return Object.values(product).map((item)=>{
        if (typeof item.type === "string"  && item.type === "fragile") {
            return fragileArray.push(item)
        }
        else if (Array.isArray(item)){
            item.map((subItem)=>{
                const subItemFragile = Object.values(subItem)[0]
                if( subItemFragile.type==="fragile"){
                    return fragileArray.push(subItemFragile)
                }
            })

        }
    })
})

console.log(fragileArray)


//Find the least and the most expensive item for a single quantity.
